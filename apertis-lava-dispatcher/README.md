# Docker Image for LAVA dispatcher (aka slave)

This is the Dockerfile to create a Docker image running the latest LAVA
Dispatcher version from the Collabora repository.

The image offers a dispatcher setup to connect a LAVA worker to a master
server with support for a QEMU device as well as hardware devices.
Communication is done with the master with encryption enabled.

Once the dispatcher is configured new device can be added to the central server
using the standard official instructions to add a new device at e.g.:
  https://lava.collabora.co.uk/static/docs/v2/first-devices.html

## System requirements

Base hardware requirements for a machine to run the dispatcher with a small set
of duts:
* 64 bit Intel compatible PC
* at least 1GB of RAM
* at least 16G of free disk space
* two network interfaces (if using physical duts with ethernet)

Software requirements:
* 64 bit Linux based operating system (e.g. Debian, Ubuntu, or RHEL).
* Docker daemon installed (tested with Docker CE 18.06.1)
* Docker compose installed (tested with Docker Compose 1.21.0)

## Host system setup

For usage of devices with Lava 3 types of connections are used:
* Network connection: For transferring test binaries
* Serial connection: For driving the test by sending commands
* Power connection: For controlling the power to the DUT

### Network setup

For usage with physical test devices aka  (Device Under Test, DUT) it is
assumed two network are in use by the system:
* An interface for using connecting to the internet/wan
* One or more interface for connecting to the Dut.

For the connection to the internet the docker image will use a standard docker
network which is masqueraded to the systems normal internet connection.

For the connection to the DUT it is assumed there is a pre-existing *bridge*
device on the host called `duts` into which interfaces connected to the DUTs
must be added bridge ports.

In case the DUTs do not have a dedicated ethernet connector, then their network
connection could be in the form of an USB connection over which the DUT
provides an RNDIS network interface. These interface need to be added by the
host system to the `duts` bridge dynamically as this is not something the
docker container can handle.

Note that no IP configuration should be done on the `duts` bridge, the docker image will
take care of that. Also please take care to *never* connect the `duts` bridge
to a public network as a DHCP server will be provided from inside the docker
image.

### Serial console setup

Lava drives the DUT via a serial line (both bootloader and running system).  No
specific serial console setup is required on the host. The only requirement on
the host is that the serial device is recognised the host (e.g. typically a
supported USB to serial adapter).

### Power control setup

To turn on/off a DUT reliably some way of controlling power in an automated
fashion is needed. The lava dispatcher docker includes
[PDUDaemon](https://github.com/pdudaemon/pdudaemon) and will thus supports all
power controllers that the included PDUDaemon supports.

No setup on the host side is required here apart from having a power controller
connected either via USB or in case of a network power controller, to ensure
it's reachable from the host.

## Docker image pre-run setup

Before running the docker image some local configuration on the docker compose
file (`docker-compose.yml`) is required:

* the `LAVA_MASTER` environment variable should be set to the address of the lava master service
  the dispatcher should connected to (e.g. `lava.collabora.co.uk` for Apertis
  dispatchers).

* the `LAVA_SLAVE` environment variable should be configured with the name of
  the dispatcher. This should be agreed with the Lava Server administrators as
  it will be the name the dispatcher is known as in the Lava system.

If the host system happens to use either the `10.0.11.1/24` or `10.0.12.1/24`
network subnets, the ip addresses configured in the compose file should
be changed not to conflict with the hosts networking.

The docker image will by default use various directories under
`/srv/lava-dispatcher` for both configuration and data storage. If some other
location for this is preferred the volumes list in the compose file should be
adapted.

## Docker image execution

To run the docker image `docker-compose` is used as this provides a convenient
way to configure how docker runs an image as opposed to specifying all options
manually on the command line. Documentation about `docker-compose` can be found
in its [manual](https://docs.docker.com/compose/overview/).

However the key comands to use in this guide are:
* `docker-compose up`: To start the container
* `docker-compose down`: To stop the container
* `docker-compose restart`: To restart he container
* `docker-compose exec dispatcher <command>`: To run `<command>` in the
  dispatcher container.

These commands should be run in the directory with the docker-compose.yml file
in it. Unless otherwise configure the container image that is will (downloaded)
and run is `docker-registry.apertis.org/apertis-lava-dispatcher`

## Docker image configuration

To finish the image configuration and setup a DUT the following needs to be
setup:
* Certificates for communication between the lava dispatcher and master
* Serial console configuration
* Power control configuration
* Board definition for the Lava master server

For each board the typical naming convention is `<device
type>-<location>-<number>`. In the examples below we're setting up a
`imx6q-sabrelite` board which is hosted in the `ehv` location.
Given it's the only `imx6q-sabrelite` board at that location it becomes
`imx6q-sabrelite-ehv-0`. Please note that regardless of the naming convention
used the name must unique name for the lava server!

The configuration process can best be started by running the docker container
once via the `docker-compose up` command. The command is expected to fail due
to missing configuration but it will create the required directory structure
under `/srv/lava-dispatcher` which can be used to drop further configuration
files as outlined in the instructions below.

### Certificate configuration

After the initial start of the image there will be a `certificates.d` directory
which will contain a keypair generated for the dispatcher. The public key
needs to be provided to the Lava server administrators 
(`<lava slave name>.key`) to allow the dispatcher to connect to the central server.

On the flip side the Lava server administrators should provide a `master.key`
file which allows the dispatcher to authenticate the master. This `master.key`
file should be placed in the `certificates.d` directory.

Once the certificates are in place the docker image can be successfully
launched and the dispatcher can connect to the master. On the dispatcher a
message indicating `INFO Master is ONLINE` can be seen if this is the case,
while in the Lava server UI the "worker" should be shown as online as well with
a very low ping time (Scheduler -> Workers menu).

### Serial console setup

For interacting with serial console the docker image includes conmux.
Configuration for conmux should be dropped in the `conmux`
subdirectory. For device serial console both the baudrate (typically 115200)
and the device name of the serial dongle should be used. It is recommended to
use persistent names in the configuration rather then e.g. `/dev/ttyUSB0`.

To make configuration simple helper program is included in the docker image called
`cu-serial` which only needs the device name and the baudrate. This program
will monitor the device node and start the `cu` program once the device node is
available.

For our test setup the persistent name of the serial dongle was:
`/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller_D-if00-port0`
while a rate of 115200 should be used. Putting that together means a
configuration was dropped named `imx6q-sabrelite-ehv-0.cf` in the
`conmux` directory with the following content:
```
listener imx6q-sabrelite-ehv-0
application console 'imx6q-sabrelite-ehv-0 console' '/usr/sbin/cu-serial /dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller_D-if00-port0 115200'
```

To apply new configuration the docker image needs to be restarted, at restart
time all the `.cf` configuration file will be applied. To test the
`conmux-console` console can be run in the container e.g. to list all the
console in the container run `docker-compose exec dispatcher conmux-console
--list` or to connect to the serial console e.g.
`docker-compose exec dispatcher conmux-console imx6q-sabrelite-ehv-0`

### Power control setup

For power control the docker image includes PDUDaemon; The configuration file
for for PDUDaemon  is expected to be found in the `pdudaemon` directory under
the name `pdudaemon.conf`. For our tests a Devantech USB RLY02 was used with
the following configuration file:
```
{
    "daemon": {
        "hostname": "127.0.0.1"
    },
    "pdus": {
        "rly02-0": {
          "driver": "devantech_USB-RLY02",
          "device": "/dev/serial/by-id/usb-Devantech_Ltd._USB-RLY02._00015636-if00"
        }
    }
}
```

To apply the configuration changes the container needs to be restarted.
Afterwards the pdu configuration can be tested by running pduclient in the
container for example to toggle the first port of the rly02 in the above
example one could run:

`docker-compose exec dispatcher pduclient --daemon localhost --hostname rly02-0 --port 1 --command reboot`

### Lava board configuration

Once all the configuration above has been done device dictionary
for the board(s) can be created. This dictionary is a simple jinja2 file, the
details for which can be found in the Lava documentation. The main per-device
configuration that is needed is the commands to connect to the serial console
(as configured above) as well as how to hard reset/power on/power off the
device. The device dictionary should be provided the Lava server administrators
to be configured in Lava. An example device dictionary for the i.mx6 sabrelite
used as an example is provided below:
```
{% extends 'imx6q-sabrelite.jinja2' %}

{% set connection_command = '/usr/bin/conmux-console imx6q-sabrelite-ehv-0' %}

{% set hard_reset_command = 'pduclient --daemon=localhost --hostname=rly02-0 --port=1 --command=reboot' %}
{% set power_off_command = 'pduclient --daemon=localhost --hostname=rly02-0 --port=1 --command=off'  %}
{% set power_on_command = 'pduclient --daemon=localhost --hostname=rly02-0 --port=1 --command=on'  %}
```

## Custom dhcp/tftp configuration

Some boards may need custom configuration on the dhcp server/tftp
configuration. For example when pulling the bootloader from the network rather
then from local storage.

The docker image uses dnsmasq as the DHCP server, configuration for this server
can be stored in the `dnsmasq.d` subdirectory. This directory will already
contain an autogenerated `dut-dhcp.conf` which should not be touched for
further configuration. Extra configuration files should be placed in this
directory with a name ending in `.conf`. These files will be read by dnsmasq on
restart.

To export extra data (e.g. bootloader) via TFTP it can be placed inside the
`tmp` subdirectory, which is the root of the TFTP server running
in the docker container.
