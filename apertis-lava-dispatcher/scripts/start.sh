#!/bin/bash
#
# Execute the LAVA Dispatcher image.
#

set -ve

if [ -z "$LAVA_MASTER" ] || [ -z "$LAVA_SLAVE" ]; then
	echo "ERROR: Missing some of the following values: LAVA_MASTER, LAVA_SLAVE"
        exit 11
fi

if [ ! -e /etc/lava-dispatcher/certificates.d/$LAVA_SLAVE.key ]; then
	echo "INFO: Generate and set slave certificates"
	/usr/share/lava-dispatcher/create_certificate.py $LAVA_SLAVE
fi

# Ensure the default gateway is set correctly
ip route del default
ip route add default via ${WAN_GATEWAY}

# Start an http file server for boot/transfer_overlay support
echo "INFO: Starting the HTTP file server"
nginx &

# Start dnsmasq for DHCP
echo "INFO: Starting dnsmasq"
echo "dhcp-range=${DHCP_RANGE},255.255.255.0,24h" > /etc/dnsmasq.d/dut-dhcp.conf
dnsmasq  -7 /etc/dnsmasq.d/   --log-facility=-

echo "INFO: Starting TFTP server"
/usr/sbin/in.tftpd --user tftp --secure --listen /var/lib/lava/dispatcher/tmp/

# Starting conmux
CONMUX_REGISTRY=/var/run/conmux-registry
touch ${CONMUX_REGISTRY}
/usr/sbin/conmux-registry 63000 ${CONMUX_REGISTRY} &

for x in /etc/conmux/*.cf ; do
  echo "Attaching $(basename $x)"
  /usr/sbin/conmux ${x} &
done

# pdudaemon for power control
pdudaemon --dbfile /run/pdu.db &

# NFS server
exportfs -a
rpcbind
rpc.statd
rpc.nfsd
rpc.mountd

# Run slave manually
echo dispatcher_ip: ${DISPATCHER_IP} > /etc/lava-dispatcher/dispatcher.yaml
echo "INFO: Starting LAVA dispatcher"
/usr/bin/lava-slave --hostname $LAVA_SLAVE \
                    --encrypt \
                    --master-cert /etc/lava-dispatcher/certificates.d/master.key \
                    --slave-cert /etc/lava-dispatcher/certificates.d/$LAVA_SLAVE.key_secret \
                    --master tcp://$LAVA_MASTER:5556 \
                    --socket-addr tcp://$LAVA_MASTER:5555 \
                    --level INFO \
                    --log-file -
