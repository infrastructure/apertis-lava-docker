This repository contains docker definition for usage with lava.

Currently only an image for the dispatcher is provided. Further instructions
for that image can be found at [its README](apertis-lava-dispatcher/README.md)
